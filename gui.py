import Tkinter as tk
import ttk
import try1
from PIL import Image, ImageTk
import os
import input_data
import shutil
import glob
from tkFileDialog import askopenfilename
import pygame
import tqdm
import subprocess

LARGE_FONT = ("Verdana", 12)
NORM_FONT = ("Verdana", 10)
SMALL_FONT = ("Verdana", 8)

###############################################################
############     Pop up message settings       ################
###############################################################

def popupmsg(msg):
    popup = tk.Tk()
    popup.wm_title("!")
    label = ttk.Label(popup, text=msg, font=NORM_FONT)
    label.pack(side="top", fill="x", pady=10)
    B1 = ttk.Button(popup, text="Okay", command=popup.destroy)
    B1.pack()
    popup.mainloop()


###############################################################
############   Popup Menu and Frame settings   ################
###############################################################

class Main(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        #tk.Tk.iconbitmap(self, default="clienticon.ico")
        tk.Tk.wm_title(self, "Generate, Analyze and Play Pieces")



        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        menubar = tk.Menu(container)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Main Menu", command=lambda: self.show_frame(StartPage))
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=quit)
        menubar.add_cascade(label="File", menu=filemenu)

        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="Help Index", command=lambda: popupmsg("Not supported just yet!"))
        helpmenu.add_command(label="About...", command=lambda: popupmsg("Not supported just yet!"))
        menubar.add_cascade(label="Help", menu=helpmenu)

        tk.Tk.config(self, menu=menubar)

        self.frames = {}

        for F in (StartPage, Generated, Analyzed, PlayMusic):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

###############################################################
###################   Main Menu        ########################
###############################################################

class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)




        self.Label1 = tk.Label(self)
        self.Label1.place(relx=0.15, rely=0.15, height=18, width=350)
        self.Label1.configure(text='''Welcome to Generating, Analyzing and Playing Pieces''',font=NORM_FONT)

        self.Label2 = tk.Label(self)
        self.Label2.place(relx=0.35, rely=0.26, height=18, width=141)
        self.Label2.configure(text='''Please select an option''')

        self.Generate = ttk.Button(self, text="Generate Music", command=lambda: controller.show_frame(Generated))
        self.Generate.place(relx=0.33, rely=0.37, height=26, width=150)

        self.Analyze = ttk.Button(self, text="Analyze Music",
                             command=lambda: controller.show_frame(Analyzed))
        self.Analyze.place(relx=0.33, rely=0.52, height=26, width=150)

        self.PlayMusic = ttk.Button(self, text="Play Music",
                             command=lambda: controller.show_frame(PlayMusic))
        self.PlayMusic.place(relx=0.33, rely=0.67, height=26, width=150)

###############################################################
############   Generate Music Menu        #####################
###############################################################

class Generated(tk.Frame):


    def start(self): #part 2 of generate

        #print("Enter song name")

        self.entry = tk.Entry(self)
        self.ok = ttk.Button(self, text="ok", command=self.on_button)
        self.ok.place(relx=0.62, rely=0.80, height=26, width=26)

        self.Label5 = tk.Label(self)
        self.Label5.place(relx=0.35, rely=0.70, height=18, width=130)
        self.Label5.configure(text='''Enter Song Name''',font=SMALL_FONT)

        self.entry.place(relx=0.33, rely=0.80, height=26, width=130)




    def generate_3(self, song_name): ### part 4 of generate
        A = song_name
        self.ok.destroy()
        self.entry.destroy()
        self.Label6 = tk.Label(self)
        self.Label6.place(relx=0.25, rely=0.70, height=18, width=200)
        self.Label6.configure(text='''New generated song :'''+ A, font=SMALL_FONT)


    def on_button(self):### part 3 of generate


        song_name = (self.entry.get())
        self.entry.delete('0', tk.END)
        filename = input_data.rename(song_name)
        if filename == 'bad':
            self.Label7 = tk.Label(self)
            self.Label7.place(relx=0.25, rely=0.70, height=18, width=200)
            self.Label7.configure(text='''Please re-enter song name''', font=SMALL_FONT)

        if filename != 'bad':
            self.generate_3(song_name)


    def generate(self): ### part 1 of generate


        input_data.generate()
        self.start()

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)



        self.Label3 = tk.Label(self)
        self.Label3.place(relx=0.35, rely=0.26, height=18, width=141)
        self.Label3.configure(text='''Generate Music''',font=LARGE_FONT)

        self.Start1 = ttk.Button(self, text="Start Process", command= self.generate)
        self.Start1.place(relx=0.33, rely=0.37, height=26, width=150)


###############################################################
#############   Analyze Music Menu        #####################
###############################################################


class Analyzed(tk.Frame):

    def analyze(self):
        res=try1.Analyze()
        res1= res.eval()
        B=res1
        comp = try1.compare(res1)
        self.result = tk.Label(self)
        self.result.place(relx=0.15, rely=0.70, height=18, width=300)
        self.result.configure(text= comp, font=SMALL_FONT)
        self.result1 = tk.Label(self)
        self.result1.place(relx=0.30, rely=0.80, height=18, width=180)
        self.result1.configure(text="Having a difference of:",font=SMALL_FONT)
        self.result2 = tk.Label(self)
        self.result2.place(relx=0.70, rely=0.80, height=18, width=50)
        self.result2.configure(text=B, font=SMALL_FONT)
        img = Image.open('compare.png')
        img.show()
        img2 = Image.open('1.png')
        img2.show()
        img3 = Image.open('2.png')
        img3.show()


    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)


        self.Label4 = tk.Label(self)
        self.Label4.place(relx=0.35, rely=0.26, height=18, width=141)
        self.Label4.configure(text='''Analyze Music''',font=LARGE_FONT)

        self.Start2 = ttk.Button(self, text="Start Process", command= self.analyze)
        self.Start2.place(relx=0.33, rely=0.37, height=26, width=150)

###############################################################
#############      Play Music Menu        #####################
###############################################################

class PlayMusic(tk.Frame):

    def openfile(self):
        filename = askopenfilename(parent=app)
        f = open(filename)
        a = len(filename)
        a = int(a) * 1000
        print a
        pygame.mixer.music.load(f)  # used to load music
        self.Start3 = ttk.Button(self, text="Start Song", command= lambda: self.play_a_progress(a))
        self.Start3.place(relx=0.33, rely=0.52, height=26, width=150)
        self.Start1 = ttk.Button(self, text="Stop Song", command=self.stopmusic)
        self.Start1.place(relx=0.33, rely=0.67, height=26, width=150)

        ###############################################################
        ########   Play generated piece           #####################
        ###############################################################
    def play_a_progress(self,a):
        self.progress(a)
        self.play()

    def progress(self,a):
        print a
        self.progress = ttk.Progressbar(self, orient="horizontal",
                                        length=200, mode="determinate")
        self.progress.pack()

        self.bytes = 0
        self.maxbytes = 0
        self.progress["value"] = 0
        self.maxbytes = a
        self.progress["maximum"] = a
        self.read_bytes()

    def play(self):

        pygame.mixer.music.play()  # play over and over, to play once (0)
        self.Start4 = ttk.Button(self, text="Pause", command=self.pausesong)
        self.Start4.place(relx=0.33, rely=0.52, height=26, width=150)


    def stopmusic(self):
        """stop currently playing music"""
        pygame.mixer.music.stop()
        self.Start3 = ttk.Button(self, text="Start Song", command=self.play)
        self.Start3.place(relx=0.33, rely=0.52, height=26, width=150)
        self.bytes = self.maxbytes + 1
        self.read_bytes()

    def pausesong(self):
        pygame.mixer.music.pause()
        paused = True
        self.Start4 = ttk.Button(self, text="Unpause", command=self.unpausesong)
        self.Start4.place(relx=0.33, rely=0.52, height=26, width=150)


    def unpausesong(self):
        pygame.mixer.music.unpause()
        self.Start4 = ttk.Button(self, text="Pause", command=self.pausesong)
        self.Start4.place(relx=0.33, rely=0.52, height=26, width=150)
        paused = False

    def read_bytes(self):
        self.bytes += 100
        self.progress["value"] = self.bytes
        if self.bytes < self.maxbytes:
            self.after(100, self.read_bytes)
        elif self.bytes == self.maxbytes or self.bytes > self.maxbytes:
              self.bytes = 0
              self.progress.destroy()

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        pygame.init()  # used to initialize pygame

        self.Label4 = tk.Label(self)
        self.Label4.place(relx=0.35, rely=0.26, height=18, width=141)
        self.Label4.configure(text='''Play Music''',font=LARGE_FONT)

        self.Start2 = ttk.Button(self, text="Start Process", command= self.openfile)
        self.Start2.place(relx=0.33, rely=0.37, height=26, width=150)


app = Main()
app.geometry("450x227+335+206")
app.title("G.A.P.P")

app.mainloop()