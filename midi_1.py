import midi
import numpy as np  # scientific computations

lowerNote = 24
upperNote = 102
length = upperNote - lowerNote  # number of piano keys


def midi_StateMatrix(midiSong, squash=True, length=length):  #changing from midi to note state matrix
    motive = midi.read_midifile(midiSong)  # motive is made up of songs

    remaining_clock = [path[0].tick for path in motive]  # array within array to process whole song
    array_1 = [0 for path in motive]  # array same size as number of songs in motive

    array_matrix = []  # declaration of array
    clock = 0

    array_2 = [[0, 0] for x in range(length)]  #  array of 78 [0,0]
    array_matrix.append(array_2)  # adding array matrix with array2
    case = True
    while case:
        if clock % (motive.resolution / 4) == (motive.resolution / 8):
            # create new array when boundary crossed
            old_array_2 = array_2
            array_2 = [[old_array_2[x][0], 0] for x in range(length)]
            array_matrix.append(array_2)
        for i in range(len(remaining_clock)):  # For each
            if not case:
                break
            while remaining_clock[i] == 0:
                path = motive[i]
                path_position = array_1[i]  # increment

                event = path[path_position]
                if isinstance(event, midi.NoteEvent):
                    if (event.pitch < lowerNote) or (event.pitch >= upperNote):
                        pass
   #--->>>remove                     # print "Note {} at clock {} out of bounds (ignoring)".format(event.pitch, clock)
                    else:
                        if isinstance(event, midi.NoteOffEvent) or event.velocity == 0:
                            array_2[event.pitch - lowerNote] = [0, 0]
                        else:
                            array_2[event.pitch - lowerNote] = [1, 1]
                elif isinstance(event, midi.TimeSignatureEvent):
                    if event.numerator not in (2, 4):
 #--->>>remove          # We don't want to worry about non-4 clock signatures. Bail early!
 #--->>>remove          # print "Found clock signature event {}. Bailing!".format(event)
                        out = array_matrix
                        case = False
                        break
                try:
                    remaining_clock[i] = path[path_position + 1].tick
                    array_1[i] += 1
                except IndexError:
                    remaining_clock[i] = None

            if remaining_clock[i] is not None:
                remaining_clock[i] -= 1

        if all(t is None for t in remaining_clock):
            break

        clock += 1

    S_matrix = np.array(array_matrix)
    array_matrix = np.hstack((S_matrix[:, :, 0], S_matrix[:, :, 1]))
    array_matrix = np.asarray(array_matrix).tolist()
    return array_matrix


def StateMatrix_midi(array_matrix, name="example", length=length):
    array_matrix = np.array(array_matrix)
    if not len(array_matrix.shape) == 3:
        array_matrix = np.dstack((array_matrix[:, :length], array_matrix[:, length:]))
    array_matrix = np.asarray(array_matrix)
    motive = midi.Pattern()
    path = midi.Track()
    motive.append(path)

    length = upperNote - lowerNote
    beat = 55

    last_clock = 0
    prev_array_2 = [[0, 0] for x in range(length)]
    for clock, array_2 in enumerate(array_matrix + [prev_array_2[:]]):
        off_key = []
        on_key = []
        for i in range(length):
            a = array_2[i]
            b = prev_array_2[i]
            if b[0] == 1:
                if a[0] == 0:
                    off_key.append(i)
                elif a[1] == 1:
                    off_key.append(i)
                    on_key.append(i)
            elif a[0] == 1:
                on_key.append(i)
        for tone in off_key:
            path.append(midi.NoteOffEvent(tick=(clock - last_clock) * beat, pitch=tone + lowerNote))
            last_clock = clock
        for tone in on_key:
            path.append(midi.NoteOnEvent(tick=(clock - last_clock) * beat, velocity=40, pitch=tone + lowerNote))
            last_clock = clock

        prev_array_2 = array_2

    end_mid = midi.EndOfTrackEvent(tick=1)
    path.append(end_mid)

    midi.write_midifile("{}.mid".format(name), motive)