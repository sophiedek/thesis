###############################################################
######         Libraries needed               #################
###############################################################

import os
import numpy as np
import glob
import tensorflow as tf
from tqdm import tqdm
import midi_1
import numpy.ma as ma
import shutil
from matplotlib.colors import colorConverter
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.image as mpimg
sess = tf.InteractiveSession()

destination = '/home/michelle/Downloads/waon-0.10/'
path = '/home/michelle/git/thesis/mich_wav/'
directory_git = '/home/michelle/git/thesis/songs1/'
song_dir1 = '/home/michelle/git/thesis/songs2/'
song_dir2 = '/home/michelle/git/thesis/songs3/'

def Analyze():
    ###############################################################
    #######Remove Previous data from WAON file and git#############
    ###############################################################

    os.chdir(destination)
    files = glob.glob('*.wav' and '*.mid')
    for filename in files:
        os.unlink(filename)

    os.chdir(directory_git)
    files = glob.glob('*.mid')
    for filename in files:
        os.unlink(filename)

    os.chdir(song_dir1)
    files = glob.glob('*.mid')
    for filename in files:
        os.unlink(filename)

    os.chdir(song_dir2)
    files = glob.glob('*.mid')
    for filename in files:
        os.unlink(filename)

    ###############################################################

    ###############################################################
    #######checking how many wav files are in directory############
    ###############################################################

    extension = '.WAV'
    extension_2 = '.midi'

    def directory(path, extension):

        list_dir = []

        list_dir = os.listdir(path)
        count = 0
        for file in list_dir:
            if file.endswith(extension):
                count += 1
        for file in list_dir:
            if file.endswith(extension_2):
                shutil.copy(path + file, directory_git)
        return count

    count = directory(path, extension)
    print count

    ###############################################################

    ###############################################################
    ######Using WAON then placing .mid in git file#################
    ###############################################################

    os.chdir('/home/michelle/Downloads/waon-0.10/')
    array = [] * count
    check = 'a'
    wav = glob.glob('{}/*.WAV*'.format(path))
    for f in wav:
        try:
            midi = np.array(f)
            array.append(midi)
            print('This is f ', f)
            os.system('./waon -i ' + f + ' -o ' + check + '.mid -w 3 -n 4096 -s 1024')
            os.system('cp ' + check + '.mid /home/michelle/git/thesis/songs1/')
            check = chr(ord(check) + 1)
        except Exception as e:
            raise e

    list_dir = os.listdir(directory_git)
    for file in list_dir:
        if file.startswith("a"):
            shutil.move(directory_git + file, song_dir1)
        if file.startswith("b"):
            shutil.move(directory_git + file, song_dir2)

    ###############################################################

    sess = tf.InteractiveSession()

    os.chdir('/home/michelle/git/thesis/')

    ###############################################################
    ######Changing from midi to note state matric #################
    ###############################################################

    def convert_songs(directory):
        files = glob.glob('{}/*.mid*'.format(directory))
        songs = []
        for f in tqdm(files):
            try:
                song = np.array(midi_1.midi_StateMatrix(f))
                if np.array(song).shape[0] > 50:
                    songs.append(song)
            except Exception as e:
                raise e
        return songs

    ###############################################################
    ######              Importing Songs           #################
    ###############################################################

    songs = convert_songs('songs2')  # These songs have already been converted from midi to msgpack
    print "{} songs processed".format(len(songs))
    songs1 = convert_songs('songs3')
    print "{} songs processed".format(len(songs1))

    ###############################################################
    ######        Getting first song data         #################
    ###############################################################


    array = ma.getdata(songs)
    print("array 0 in binary form (array)")
    print(array)  # array form
    array = tf.squeeze(array)
    d = type(array)
    print("this is songs (d)", d)
    m = tf.shape(array)
    print("shape of array 0 (m)")
    print(m.eval())  # ex [ 1 257 156]
    m = tf.reduce_sum(m)
    print("total size of array 0(m)")
    m = (m.eval())  # array  # ex sum = 414

    ###############################################################
    ######        Getting second song data        #################
    ###############################################################

    array1 = ma.getdata(songs1)
    print("array 1 in binary form(array1)")
    print(array1)
    array1 = tf.squeeze(array1)
    print(array1)
    z = tf.shape(array1)
    print("shape of array 1(z)")
    print(z.eval())
    z = tf.reduce_sum(z)
    print("total size of array 1(z)")
    z = (z.eval())  # arr ay1

    ###############################################################
    ######       Adding zero padding at the end   #################
    ###############################################################
    i = 0
    while m > z and i == 0:
        zero_padding1 = tf.zeros([(m - 156) - (z - 156), 156], dtype=tf.int64)
        j = tf.shape(zero_padding1)
        j = tf.reduce_sum(j)
        print(j.eval())
        # print("bye")
        array1 = tf.concat(0, [array1, zero_padding1])
        i = i + 1

    while z > m and i == 0:
        zero_padding1 = tf.zeros([(z - 156) - (m - 156), 156], dtype=tf.int64)
        j = tf.shape(zero_padding1)
        j = tf.reduce_sum(j)
        print(j.eval())
        array = tf.concat(0, [array, zero_padding1])
        # print("hello")
        i = i + 1

    ###############################################################
    ######       Subtracting arrays               #################
    ###############################################################
    X = array.eval()
    Y = array1.eval()

    color1 = colorConverter.to_rgba('white')
    color2 = colorConverter.to_rgba('Red')

    # make the colormaps
    cmap1 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap', ['Black', 'blue'], 256)
    cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2', [color1, color2], 256)

    cmap2._init()  # create the _lut array, with rgba values

    # create your alpha array and fill the colormap with them.
    # here it is progressive, but you can create whathever you want
    alphas = np.linspace(0, 0.8, cmap2.N + 3)
    cmap2._lut[:, -1] = alphas

    arr2 = plt.imshow(X, interpolation='nearest', cmap=cmap1, origin='lower')
    arr3 = plt.imshow(Y, interpolation='nearest', cmap=cmap2, origin='lower')

    #plt.show()

    plt.savefig('compare.png')
    ########try maybe to do X - Y to remove similarities therefor each time one practices can see if doing better if there
    # are less dots??


    plt.matshow(X, cmap='Blues')
    plt.savefig('1.png')
    plt.matshow(Y, cmap='Reds')
    plt.savefig('2.png')
########################################







    ###############################################################
    m = tf.shape(array)
    print("shape of array 0 (m)")
    p = (m.eval())
    z = tf.shape(array1)
    print("shape of array 1 (z)")
    print(z.eval())
    result = tf.sub(array, array1)
    print(result.eval())
    result = abs(result)
    print(result.eval())
    result = tf.reduce_sum(result)
    print(result.eval())

    ###############################################################
    ######           Concluding result            #################
    ###############################################################
    result = tf.cond(result <= 100, lambda: tf.Print(result, [result], message="This is good: "),
                     lambda: tf.Print(result, [result], message="This is not good: "))
    # Add more elements of the graph using a
    output = tf.add(result, result).eval()
    os.system('clear')
    return(result)

def compare(res1):
    if (res1<=50):
        comp = "This is excellent"
        return (comp)
    elif ((res1>50)&(res1<=100)):
        comp = "This is good"
        return (comp)
    elif ((res1>100)&(res1<=200)):
        comp = "This is fairly good"
        return (comp)
    elif ((res1>200)&(res1<=500)):
        comp = "This is bad"
        return (comp)
    elif (res1>500):
        comp = "Different Song"
        return (comp)
