import os
import tqdm
import numpy as np
import glob
import midi_1
from tqdm import tqdm
import tensorflow as tf
from shutil import copy
from tensorflow.python.ops import control_flow_ops
from numpy import ma
import pygame
import shutil


destination = '/home/michelle/Downloads/waon-0.10/'
path = '/home/michelle/git/thesis/wav_songs/'
directory_git = '/home/michelle/git/thesis/songs/'
songv= '/home/michelle/git/thesis/'
generated = '/home/michelle/git/thesis/generated/'
path1 = "/home/michelle/git/thesis/final_pieces/"

def generate():
###############################################################
######Remove Previous data from WAON file,git and generated ###
###############################################################

    os.chdir(destination)
    files=glob.glob('*.wav'and'*.mid')
    for filename in files:
        os.unlink(filename)

    os.chdir(directory_git)
    files=glob.glob('*.mid')
    for filename in files:
        os.unlink(filename)


###############################################################

###############################################################
#######checking how many wav files are in directory############
###############################################################

    extension = '.wav'
    extension_2 = '.mid'

    def directory(path,extension):
        list_dir = []
        list_dir = os.listdir(path)
        count = 0
        for file in list_dir:
            if file.endswith(extension):
                count += 1
        for file in list_dir:
            if file.endswith(extension_2):
                copy(path+file,directory_git)
        return count

    count = directory(path,extension)
    print count

###############################################################

###############################################################
#######Using WAON then placing .mid in git file################
###############################################################

    os.chdir('/home/michelle/Downloads/waon-0.10/')
    array = [] * count
    midi_format = 'a'
    wav = glob.glob('{}/*.wav*'.format(path))
    for f in wav:
       try:
          midi = np.array(f)
          array.append(midi)
          print('This is f ', f)
          os.system('./waon -i ' + f  + ' -o ' + midi_format + '.mid -w 3 -n 4096 -s 2048')
          os.system('cp ' + midi_format +'.mid /home/michelle/git/thesis/songs/')
          midi_format = chr(ord(midi_format) + 1)
       except Exception as e:
        raise e

###############################################################

###############################################################
#######Converting midi to note state matrix ###################
###############################################################

    os.chdir('/home/michelle/git/thesis/')

    def song_path(directory_git):
        songs = glob.glob('{}/*.mid*'.format(directory_git))
        array = []
        for f in tqdm(songs):#for loop
            try:
                list = np.array(midi_1.midi_StateMatrix(f))
                if np.array(list).shape[0] > 50:
                    array.append(list)
            except Exception as e: #So the program does not crash
                raise e           #Displays errors
        return array

    list = song_path('song') #Already convertered
    print ("{} Processed".format(len(list)))

###############################################################

###############################################################
#############    Neural Network Variables   ###################
###############################################################

    upperNote = midi_1.upperNote
    lowerNote = midi_1.lowerNote
    length = upperNote -lowerNote# number of piano keys

    time_steps = 15 #Timesteps allocated
    hidden_layers = 50 #Hidden layers value
    visible_layers = 2*length*time_steps #Visible Layers value

    inputs = tf.placeholder(tf.float32, [None,visible_layers], name="inputs") #Input data of NN
    Weights = tf.Variable(tf.random_normal([visible_layers , hidden_layers], 0.01), name="W") #Weights of NN
    visible_bias = tf.Variable(tf.zeros([1, visible_layers],  tf.float32, name="vb")) #Visible Layer Bias Vector
    hidden_bias = tf.Variable(tf.zeros([1, hidden_layers],  tf.float32, name="hb")) #Hidden Layer Bias Vector

    training_size = 100 #Training size allocated at same time
    epochs = 200 #epochs size
    learning_rate = tf.constant(0.005, tf.float32)
###############################################################

###############################################################
#############    Return 0's and 1's elements ##################
###############################################################

    def element(odds):
        return tf.floor(odds + tf.random_uniform(tf.shape(odds), 0, 1)) #sample from the data given (odds)
###############################################################

###############################################################
########  Markov chain monte carlo algorithm ##################
###############################################################

    def MCMC(step): # MCMC chain distributing by the inputs, weights and biases
        def MCMC_step(sum, step, visible_step): # processes one MCMC step, visible layer
            # with the visible part, sample the hidden and vs versa
            hidden_step = element(tf.sigmoid(tf.matmul(visible_step, Weights) + hidden_bias))
            visible_step = element(tf.sigmoid(tf.matmul(hidden_step, tf.transpose(Weights)) + visible_bias))
            return sum + 1, step, visible_step

        counter = tf.constant(0)
        [_, _, element_x] = control_flow_ops.While(lambda sum, num_iter, *args: sum < num_iter,
                                                  MCMC_step, [counter, tf.constant(step), inputs], 1, False)
        # so the program does not re enter the MCMC step
        element_x = tf.stop_gradient(element_x)
        return element_x

##########################Train the MCMC#######################

    element_x = MCMC(1)
    hidden_elem = element(tf.sigmoid(tf.matmul(inputs, Weights) + hidden_bias))#nodes in the hidden elem starting from input
    hidden_elem2 = element(tf.sigmoid(tf.matmul(element_x, Weights) + hidden_bias))#nodes in the hidden elem starting from elem inputs

#########################Update Neural Network variables#######

    dim_size = tf.cast(tf.shape(inputs)[0], tf.float32)
    add_Weights = tf.mul(learning_rate/dim_size, tf.sub(tf.matmul(tf.transpose(inputs), hidden_elem),
                                                    tf.matmul(tf.transpose(element_x), hidden_elem2)))
    add_visible_bias = tf.mul(learning_rate/dim_size, tf.reduce_sum(tf.sub(inputs, element_x), 0, True))
    add_hidden_bias = tf.mul(learning_rate/dim_size, tf.reduce_sum(tf.sub(hidden_elem, hidden_elem2), 0, True))

    new_W = Weights.assign_add(add_Weights)
    new_VB =  visible_bias.assign_add(add_visible_bias)
    new_HB = hidden_bias.assign_add(add_hidden_bias)
    updated = [new_W,new_VB,new_HB ]

###############################################################

###############################################################
########   Train model & init variables   #####################
###############################################################

    with tf.Session() as sess:
        initialize = tf.initialize_all_variables()
        sess.run(initialize)
        for epoch in tqdm(range(epochs)): #process all the data a number of times from epochs
            for score in list: #reshape
                score = np.array(score)
                score = score[:np.floor(score.shape[0]/time_steps)*time_steps]
                score = np.reshape(score, [score.shape[0]/time_steps, score.shape[1]*time_steps])
                #depending on training_size value the training will take place at one score at a time
                for j in range(1, len(score), training_size):
                    train = score[j:j+training_size]
                    sess.run(updated, feed_dict={inputs: train})
        array2 = np.ndarray([0, 156])
####### Trained model turned into music##################

#carry out a MCMC when the nodes in the visible layer are starting from 0
        element = MCMC(1).eval(session=sess, feed_dict={inputs: np.zeros((10, visible_layers))})
        for i in range(element.shape[0]):
            if not any(element[i,:]):
                continue

####### Trained model turned into music##################
# Now reshape by multiplying the time with length, and save file in format midifile
#Save all the generated pieces into one piece
            S_matrix = np.reshape(element[i,:], (time_steps, 2*length))
            S1 = tf.shape(S_matrix)
            print('Total music shape', S1.eval())
            array = ma.getdata(S_matrix)
            print(array)
            array1 = tf.shape(array)
            print(array1.eval())
            array = np.array(array)
            a = type(array)
            print(a)
            b = type(array2)
            print(b)
            array2 = np.concatenate((array2, array))
            print('This is  = ', array2)
            array3 = tf.shape(array2)
            print('Total array shape', array3.eval())
            midi_1.StateMatrix_midi(array2, "/home/michelle/git/thesis/generated/Gen{}".format(i))



###############################################################

###############################################################
########   Play generated piece           #####################
###############################################################
#os.chdir('/home/michelle/git/thesis/generated/')
#pygame.init()  # used to initialize pygame
#pygame.mixer.music.load('/home/michelle/git/thesis/generated/Gen9.mid')  # used to load music
#pygame.mixer.music.play(0)  # play over and over, to play once (0)

#clock = pygame.time.Clock()
#while pygame.mixer.music.get_busy():
    # check if playback has finished
#    clock.tick(30)
    # clock is used to hear the music as a loop. If no clock no sound
###############################################################

###############################################################
########   Rename generated piece         #####################
###############################################################

#song_name = raw_input('Enter song name: ')
#list_dir = os.listdir(path1)
#for f in (list_dir):
#    if f.startswith(song_name):
#        print("Filename already taken")
#        song_name = raw_input('Please re-enter song name: ')

#print('New song = ', song_name)
#for dpath, dnames, fnames in os.walk("/home/michelle/git/thesis/generated/"):
#    for f in fnames:
#        os.chdir(dpath)
#        if f.startswith('Gen9'):
#            os.rename(f, f.replace('Gen9', song_name))
#            shutil.move(generated + song_name + ".mid","/home/michelle/git/thesis/final_pieces/")
#            files = glob.glob('*.mid')
#            for filename in files:
#                os.unlink(filename)